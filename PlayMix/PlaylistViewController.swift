//
//  PlaylistViewController.swift
//  PlayMix - třída pro zobrazení položek z vybraného playlistu ze zařízení iOS / nelze použít v simulátoru
//
//  Created by Jan Sevela on 14/04/16.
//  Copyright © 2016 Jan Sevela. All rights reserved.
//

import Foundation
import UIKit
import MediaPlayer

class PlaylistViewController : PlaylistsViewController{
    
    var thisPlaylist:NSDictionary?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "PlaylistCell")
        self.navigationItem.title = thisPlaylist!["playlist"] as? String
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        print("viewDidAppear-PlaylistViewController")
        self.tableView.reloadData()
    }
    
    /**
    Data tableview
    */
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let count = (thisPlaylist!["songs"]?.count)! as Int
        if count > 0 {
            return count
        }
        return 0
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let tableViewCell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier("PlaylistCell") as UITableViewCell!
        
        let songsOfPlaylist = thisPlaylist!["songs"] as? [NSDictionary]
        let song =  songsOfPlaylist![indexPath.row]
        let songName:String = (song["title"] as? String)!
        let songArtist:String = (song["artist"] as? String)!
        tableViewCell.textLabel?.text = "\(songArtist) - \(songName)"
        return tableViewCell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("Row \(indexPath.row) selected")
        //performSegueWithIdentifier("goToPlayerViewController2", sender: self)
        self.playAndGoToPlayerVC(indexPath)
    }
    
    /**
     připrava na přidání skladby do fronty AVQueuePlayer
     */
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        print("PrepareForSegue")
        if segue.identifier == "goToPlayerViewController20"{
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let songIndex = indexPath.row
                let song = (thisPlaylist!["songs"] as! NSArray).objectAtIndex(songIndex) as! NSDictionary
                
                // initialize another view controller and cast it as your view controller
                let tabBarVC = segue.destinationViewController as! UITabBarController
                let PlayerVC = tabBarVC.viewControllers?.first as! PlayerViewController
                
                gl.playingMode = 1 //gl.playingMode = song from playlist
                PlayerVC.playSongWithId(song["songId"] as! NSNumber/*, title:song["title"] as! String, artist:song["artist"] as! String*/)
            }
        }
    }
    /**
    Spuštění přehrávání skladby streamu
    */
    func playAndGoToPlayerVC(index: NSIndexPath?) {
        if let indexPath = index {
            gl.playingMode = 1 //gl.playingMode = režim přehrávání písničky z playlistu
            
            let songIndex = indexPath.row
            let song = (thisPlaylist!["songs"] as! NSArray).objectAtIndex(songIndex) as! NSDictionary
            
            // initializace PlayerViewControlleru
            let PlayerVC = self.tabBarController!.viewControllers![0] as! PlayerViewController

            PlayerVC.playSongWithId(song["songId"] as! NSNumber)
            if(gl.avQueuePlayer.rate == 0){
                PlayerVC.play()
            }
            self.tabBarController?.selectedIndex = 0
        }
    }
    
    

}
