//
//  Musicquery.swift
//  PlayMix - třída pro otázky ohledně položek z itunes knihovny na iOS zařízení
//
//  Created by Jan Sevela on 19/03/16.
//  Copyright © 2016 Jan Sevela. All rights reserved.
//
import MediaPlayer

/**
 Třída pro dotazy na položky z itunes knihovny na iOS zařízení
 */
class MusicQuery: NSObject {
    
    /**
     Dotazy na skladby v zařízení (nefunguje na simulátoru)
     
     - parameter completionHandler: closure blok s výsledkem
     - dictionary: struktura
     - playlists: pole dictionaries
     - playlistCount: celkem playlistů na zařízení
     
     */
    func queryForPlaylists(completionHandler:(result:NSDictionary) -> Void) {
        
        let tableData = MPMediaQuery.playlistsQuery()
        
        var playlistCount = 0
        var playlists:[AnyObject] = [AnyObject]()
        let playlistSortingDictionary:NSMutableDictionary = NSMutableDictionary()
        
        let MPPlaylists = tableData.collections! as [MPMediaItemCollection]
        
        for MPPlaylist in MPPlaylists {
            
            let playlistName = MPPlaylist.valueForProperty(MPMediaPlaylistPropertyName) as! String
            let playlistSongs = MPPlaylist.items as [MPMediaItem]
            
            for song in playlistSongs {
                
                let artistName:String = song.valueForProperty(MPMediaItemPropertyArtist) as? String ?? ""
                let songTitle:String = song.valueForProperty(MPMediaItemPropertyTitle) as? String ?? ""
                let songId:NSNumber = song.valueForProperty(MPMediaItemPropertyPersistentID) as! NSNumber
                
                var songPlaylist = playlistSortingDictionary.objectForKey(playlistName) as? NSDictionary
                if (songPlaylist == nil) {
                    songPlaylist = NSDictionary(objects: [playlistName,NSMutableArray()], forKeys: ["playlist","songs"])
                    playlistSortingDictionary[playlistName] = songPlaylist
                }
                let songs:NSMutableArray = songPlaylist!["songs"] as! NSMutableArray
                let song:NSDictionary = ["title":songTitle,"artist":artistName ,"songId":songId]
                songs.addObject(song)
            }
            
            playlistCount += 1
            
            //seřazení podle názvu playlistů
            let sortedPlaylistsByName = playlistSortingDictionary.keysSortedByValueUsingComparator { (obj1:AnyObject! , obj2:AnyObject!) -> NSComparisonResult in
                let one = obj1 as! NSDictionary
                let two = obj2 as! NSDictionary
                return (one["playlist"] as! String).localizedCaseInsensitiveCompare((two["playlist"] as! String))
            }
            
            for playlist in sortedPlaylistsByName {
                let songsPlaylist: AnyObject? = playlistSortingDictionary[(playlist as! String)]
                playlists.append(songsPlaylist!)
            }
            
            playlistSortingDictionary.removeAllObjects()
        }
        
        completionHandler(result: NSDictionary(objects: [playlists, playlistCount], forKeys: ["playlists", "playlistCount"]))
    }
    
    /**
     Dotaz na skladbu podle ID skladby
     - parameter songId:            songPerstistenceId
     - parameter completionHandler: MPMediaItem jediná instance
     */
    func queryForSongWithId(songId:NSNumber, completionHandler:(result:MPMediaItem?) -> Void) {
        let mediaItemPersistenceIdPredicate:MPMediaPropertyPredicate =
        MPMediaPropertyPredicate(value: songId, forProperty: MPMediaItemPropertyPersistentID)
        let songQuery = MPMediaQuery(filterPredicates: NSSet(object: mediaItemPersistenceIdPredicate) as? Set<MPMediaPredicate>)
        completionHandler(result: songQuery.items!.last as MPMediaItem!)
    }
    
    
    
}
