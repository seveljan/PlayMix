//
//  PlaylistsViewController.swift
//  PlayMix - třída pro zobrazení všech playlistů z iTunes knihovny na iOS zařízení / nelze použít v simulátoru
//
//  Created by Jan Sevela on 13/02/16.
//  Copyright © 2016 Jan Sevela. All rights reserved.
//

import Foundation
import UIKit
import MediaPlayer

class PlaylistsViewController : UITableViewController {
    
    var playlists = [NSDictionary]()
    
    override func viewDidLoad() {
        print("viewDidLoad-MixesViewController")
        super.viewDidLoad()
        self.queryPlaylists()
        self.title = "Playlisty(\(self.playlists.count))"
        self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "PlaylistsCell")
        self.tableView.reloadData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        print("viewWillAppear-MixesViewController")
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        print("viewWillDisappear-MixesViewController")
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        print("viewDidAppear-PlaylistsViewController")
        self.tableView.reloadData()
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int  {
        return self.playlists.count ?? 0
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell     {
        let tableViewCell: UITableViewCell = tableView.dequeueReusableCellWithIdentifier("PlaylistsCell") as UITableViewCell!
        
        let thisPlaylist:NSDictionary = self.playlists[indexPath.row] as NSDictionary
        tableViewCell.textLabel?.text = thisPlaylist["playlist"] as? String
        
        return tableViewCell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("Row \(indexPath.row) selected")
        performSegueWithIdentifier("goToPlaylistViewController", sender: self)
    }
    

    /**
     Příprava na zobrazení skladeb vybraného playlistu
     */
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        print("PrepareForSegue")
        //NSThread.sleepForTimeInterval(1)
        if segue.identifier == "goToPlaylistViewController"{
            if let indexPath = self.tableView.indexPathForSelectedRow {
                // initialize new view controller and cast it as your view controller
                let PlaylistVC = segue.destinationViewController as! PlaylistViewController
                // your new view controller should have property that will store passed value
                PlaylistVC.thisPlaylist = self.playlists[indexPath.row] as NSDictionary
            }
        }
    }
    
    /**
     Načtení informací o playlistech ze systémové hudební knihovny
     */
    private func queryPlaylists() {
        
        title = "Searching..."
        MusicQuery().queryForPlaylists {(result:NSDictionary?) in
            if let nonNilResult = result {
                self.playlists = nonNilResult["playlists"] as! [NSDictionary]
            }
        }
    }
}