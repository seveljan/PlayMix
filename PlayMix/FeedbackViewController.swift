//
//  FeedbackController.swift
//  PlayMix - třídá pro řízení zobrazení obrazovky pro zpětnou vazbu
//
//  Created by Jan Sevela on 16/05/16.
//  Copyright © 2016 Jan Sevela. All rights reserved.
//

import UIKit
import MessageUI


class FeedbackViewController : UIViewController, MFMailComposeViewControllerDelegate {
    
    @IBAction func SendFeedback(sender: AnyObject) {
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.presentViewController(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("viewDidLoad-FeedbackViewController")

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        print("viewWillAppear-FeedbackViewController")
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        print("viewWillDisappear-FeedbackViewController")
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        print("viewDidAppear-FeedbackViewController")
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients(["jan@sevela.cz"])
        mailComposerVC.setSubject("Zpětná vazba z aplikace PlayMix")
        mailComposerVC.setMessageBody("Našel jsem chybu a nebo mám nápad...", isHTML: false)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        let alert = UIAlertController(title: "E-mail nelze odeslat!", message: "Tvoje zažízení nemůže odeslat e-mail.  Zkontroluj konfiguraci e-mailu a zkus to znovu.", preferredStyle: UIAlertControllerStyle.Alert)
        let ok = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel) { (action :UIAlertAction) -> Void in
        }
        alert.addAction(ok)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
}
