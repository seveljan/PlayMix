//
//  MixAddItemsController.swift
//  PlayMix - třída pro přidávání dalších položek do vybraného mixu
//
//  Created by Jan Sevela on 23/04/16.
//  Copyright © 2016 Jan Sevela. All rights reserved.
//

//import Foundation
import UIKit

class MixAddItemsViewController : UITableViewController {
    
    var showPlaylists: Bool = true
    var playlists = [NSDictionary]()
    var mixID: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        if(showPlaylists){
            self.title = "Playlisty"
            self.queryPlaylists()
        }
        else{
            self.title = "Rádia"
            
        }
        print("mixID: "+String(mixID))
        let addMixesButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: #selector(MixAddItemsViewController.addItemsAction(_:)))
        self.navigationItem.rightBarButtonItem = addMixesButton
        self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "MixAddItemsCell")
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        print("viewDidAppear-MixAddItemsViewController")
        
    }
    
    /**
    Data tableview
    */
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(showPlaylists){
            return playlists.count ?? 0
        }
        return mainRadios.count ?? 0
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("MixAddItemsCell") as UITableViewCell!
        
        if(showPlaylists){
            let thisPlaylist:NSDictionary = self.playlists[indexPath.row] as NSDictionary
            cell.textLabel?.text = thisPlaylist["playlist"] as? String
        }
            
        else{
            cell.textLabel?.text = mainRadios[indexPath.row].sectionName
        }
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        print("Select "+String(indexPath.row))
        // Obnovení názvu tlačítka zpět s informací o počtu vybraných položek
        self.updateButtonsToMatchTableState()
    }
    
    override func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        // Obnovení názvu tlačítka zpět s informací o počtu vybraných položek
        self.updateBackButtonTitle()
    }
    
    /**
    Akce - přidání vybaných položek
    */
    func addItemsAction (sender: AnyObject) {
        // Otevření allert, typu ActionSheet, dialogu s OK a zrušit tlačítky
        let actionTitle: String
        if self.tableView.indexPathsForSelectedRows?.count ?? 0 == 1 {
            actionTitle = NSLocalizedString("Jsi si jist, že chceš přidat jen tuto položku?", comment: "")
            
        } else {
            actionTitle = NSLocalizedString("Jsi si jist, že chceš přidat tyto položky?", comment: "")
        }
        let cancelTitle = NSLocalizedString("Zrušit", comment: "Nadpis zrušit pro akci přidání položek")
        let okTitle = NSLocalizedString("OK", comment: "Nadpis OK pro akci přidání položek")
        
        let actionAlert = UIAlertController(title: actionTitle, message: nil, preferredStyle: .ActionSheet)
        actionAlert.popoverPresentationController?.sourceView = self.view
        
        let cancelAction = UIAlertAction(title: cancelTitle, style: .Cancel) {_ in
            self.actionSheetClickedButtonAtIndex(1)
        }
        let okAction = UIAlertAction(title: okTitle, style: .Default) {_ in
            self.actionSheetClickedButtonAtIndex(0)
            //přechod na předchozí controller
            self.navigationController?.popViewControllerAnimated(true)
        }
        actionAlert.addAction(cancelAction)
        actionAlert.addAction(okAction)
        self.presentViewController(actionAlert, animated: true, completion: nil)
    }
    
    /**
    Akce - kliknutí na položky allertu typu ActionSheet
     
     - parameter buttonIndex: index kliknutého tlačítka
    */
    func actionSheetClickedButtonAtIndex(buttonIndex: Int) {
        if buttonIndex == 0 {
            // Přidání vybraných položek
            if let selectedRows = self.tableView.indexPathsForSelectedRows{
                let addSpecificRows = selectedRows.count > 0
                if addSpecificRows {
                    for selectionIndex in selectedRows {
                        if(showPlaylists){
                            //let thisIsIt:NSDictionary = self.playlists[selectionIndex.row] as NSDictionary
                            //print(audioMixes.items[mixID!].mixName)
                            //NSThread.sleepForTimeInterval(10)
                            gl.audioMixes.items[mixID!].mixQueue.push(audioMixItem(mixItem: self.playlists[selectionIndex.row], mixItemDuration: 0, mixItemPauseDuration: 0, mixItemRepeat:0))
                        }
                        else{
                            gl.radioMixes.items[mixID!].mixQueue.push(radioMixItem(mixItem: mainRadios[selectionIndex.row], mixItemDuration: 0, mixItemPauseDuration: 0, mixItemRepeat: 0))
                        }
                    }
                } else {
                // nebylo nic vybráno
                }
            }
        }
    }
    
    /**
    Obnovení tlačítek navigation baru - reagování na výběr položek uživatele
    */
    private func updateButtonsToMatchTableState() {
//        if self.tableView.editing {
//            // Show the option to cancel the edit.
//            self.navigationItem.rightBarButtonItem = self.cancelButton
//            self.updateDeleteButtonTitle()
//            // Show the delete button.
//            self.navigationItem.leftBarButtonItem = self.deleteButton
//        } else {
//            // Not in editing mode.
//            self.navigationItem.leftBarButtonItem = self.addButton
//            //        // Show the edit button, but disable the edit button if there's nothing to edit.
//            self.editButton.enabled = !self.dataArray.isEmpty
//            self.navigationItem.rightBarButtonItem = self.editButton
//        }
    }
    
    /**
     Obnovení tlačítek navigation baru - reagování na výběr položek uživatele
     */
    private func updateBackButtonTitle() {
//        // Update the delete button's title, based on how many items are selected
//        let selectedRows = self.tableView.indexPathsForSelectedRows ?? []
//        let allItemsAreSelected = selectedRows.count == self.dataArray.count
//        let noItemsAreSelected = selectedRows.isEmpty
//        if allItemsAreSelected || noItemsAreSelected {
//            self.deleteButton.title = NSLocalizedString("Delete All", comment: "")
//        } else {
//            let titleFormatString =
//                NSLocalizedString("Delete (%d)", comment: "Title for delete button with placeholder for number")
//            self.deleteButton.title = String(format: titleFormatString, Int32(selectedRows.count))
//        }
    }
    
    /**
    Dotaz na zjištění playlistů z knihovny iTunes na zařízení pomocí knihovny MediaPlayer
    */
    private func queryPlaylists() {
        
        MusicQuery().queryForPlaylists {(result:NSDictionary?) in
            if let nonNilResult = result {
                self.playlists = nonNilResult["playlists"] as! [NSDictionary]
            }
        }
    }



}