//
//  Ttems.swift
//  PlayMix - Datové struktury a inicializace globálních proměnných
//
//  Created by Jan Sevela on 14/02/16.
//  Copyright © 2016 Jan Sevela. All rights reserved.
//
import AVFoundation

/**
 Globální konstanty a proměnné
 */
struct gl {
    static let avQueuePlayer:AVQueuePlayer = AVQueuePlayer()    //přehrávač
    static var timer:NSTimer?                                   //časovač
    static var radioMixes = Queue<radioMix>()                   //mixy typu rádio streamy
    static var audioMixes = Queue<audioMix>()                   //mixy typu playlisty
    static var radiosMix: radioMix?                             //pomocná struktura pro uložení aktuálního mixu typu rádio streamy
    static var playlistsMix: audioMix?                          //pomocná struktura pro uložení aktuálního mixu typu playlisty
    static var volumeBefore: Float = 0                          //pomocná položka pro uloženení předchozího stavu hlasitosti
    static var playingMode: Int = 0                             //mód přehrávání 0=výchozí 1=písnička z playlistu, 2=rádio stream, 3=mix playlistů, 4=mix rádio streamů
}

/**
 Struktura rádio streamu
 */
struct RadioItem {
    let sectionName : String!
    let sectionType : String!
    let sectionURL : NSURL!
}

/**
 Přednastavené radio streamy
 */
var mainRadios = [RadioItem(sectionName: "Tip Rádio AAC 64kbps(cca 28MB/h) ",sectionType: "Radio", sectionURL: NSURL(string: "http://mp3stream3.abradio.cz:8000/tipradio64.aac")),
                  RadioItem(sectionName: "SeeJay Radio AAC 64kbps(cca 28MB/h)",sectionType: "Radio", sectionURL: NSURL(string: "http://live.seejay.cz/seejay64.aac"))]

/**
 Univerzální fronta
 */
struct Queue<Element> {
    var items = [Element!]()
    mutating func push(item: Element) {
        items.append(item)
    }
    mutating func pop() -> Element {
        return items.removeFirst()!
    }
    mutating func front() -> Element {
        return items.first!
    }
    mutating func size() -> Int {
        return items.count
    }
    mutating func clear() {
        items.removeAll()
    }
    mutating func at(pos: Int) -> Element{
        return items[pos]!
    }
}

/**
 Struktura položky z radiomixu
 */
struct radioMixItem {
    var mixItem: /*UnsafeMutablePointer<*/RadioItem//>
    var mixItemDuration: Float64
    var mixItemPauseDuration: Float64
    var mixItemRepeat: Int
}

/**
 Struktura radiomixu
 */
struct radioMix {
    var mixName : String!
    var mixQueue = Queue<radioMixItem>()
    var mixGlobalDuration: Float64
    var mixGlobalPauseDuration: Float64
    var mixGlobalItemRepeat: Int
    var mixGlobalSettingsON: Bool
}

/**
 Struktura položky z audioMixu
 */
struct audioMixItem {
    var mixItem: NSDictionary
    var mixItemDuration: Float64
    var mixItemPauseDuration: Float64
    var mixItemRepeat: Int
}

/**
 Struktura audioMixu
 */
struct audioMix {
    var mixName: String!
    var mixQueue = Queue<audioMixItem>()
    var mixGlobalDuration: Float64
    var mixGlobalPauseDuration: Float64
    var mixGlobalItemRepeat: Int
    var mixGlobalSettingsON: Bool
}




