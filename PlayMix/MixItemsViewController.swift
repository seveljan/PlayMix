//
//  MixPlaylistsViewController.swift
//  PlayMix - třída pro zobrazení všech položek vybraného mixu
//
//  Created by Jan Sevela on 21/04/16.
//  Copyright © 2016 Jan Sevela. All rights reserved.
//

//import Foundation
import UIKit

class MixItemsViewController : UITableViewController {
    
    var thisItemPlaylists: audioMix?
    var thisItemRadios: radioMix?
    var mixID: Int!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        print("viewDidLoad-MixesItemsViewController")
        if((thisItemRadios) != nil){
            self.title = gl.radioMixes.items[mixID!].mixName
        }
        else{
            self.title = gl.audioMixes.items[mixID!].mixName
        }
        let addItemsButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Add, target: self, action: #selector(MixItemsViewController.addItemsAction(_:)))
        self.navigationItem.rightBarButtonItem = addItemsButton
        self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "MixItemsCell")
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        print("viewWillAppear-MixesItemsViewController")
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        print("viewWillDisappear-MixesItemsViewController")
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        print("viewDidAppear-MixesItemsViewController")
        if((thisItemRadios) != nil){
            thisItemRadios = gl.radioMixes.items[mixID!]
        }
        else{
            thisItemPlaylists = gl.audioMixes.items[mixID!]
        }
        print("size: "+String(thisItemRadios?.mixQueue.size()))
        self.tableView.reloadData()
    }
    
    /**
    Data tableview
    */
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if((thisItemRadios) != nil){
            return (thisItemRadios?.mixQueue.size())!+1 //je tam i položka nastavení
        }
        return (thisItemPlaylists?.mixQueue.size())!+1  //je tam i položka nastavení
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("MixItemsCell") as UITableViewCell!
        
        if (indexPath.row == 0) {
            if((thisItemPlaylists) != nil){
                if(thisItemPlaylists?.mixGlobalSettingsON == true){
                    cell.backgroundColor = UIColor.greenColor()
                    cell.textLabel?.text = "Globalní nastavení: ON"
                }
                else{
                    cell.backgroundColor = UIColor.redColor()
                    cell.textLabel?.text = "Globalní nastavení: OFF"
                }
            }
            else{
                if(thisItemRadios?.mixGlobalSettingsON == true){
                    cell.backgroundColor = UIColor.greenColor()
                    cell.textLabel?.text = "Globalní nastavení: ON"
                }
                else{
                    cell.backgroundColor = UIColor.redColor()
                    cell.textLabel?.text = "Globalní nastavení: OFF"
                }
            }
            print("GlobalSettings")
            return cell
        }
        else{
            if((thisItemRadios) != nil){
                cell.textLabel?.text = thisItemRadios?.mixQueue.at(indexPath.row-1).mixItem.sectionName
                print(cell.textLabel?.text)
            }
                
            else{
                cell.textLabel?.text = thisItemPlaylists?.mixQueue.at(indexPath.row-1).mixItem["playlist"] as? String
            }
        }
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        performSegueWithIdentifier("goToMixSettings", sender: self)
        print("Select "+String(indexPath.row))
    }
    
    /**
    Akce - přidání položek mixu
    */
    func addItemsAction (sender: AnyObject) {
        performSegueWithIdentifier("goToMixAddItemsController", sender: self)
    }
    
    /**
    Příprava pro segue
    */
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        print("PrepareForSegue")
        if segue.identifier == "goToMixAddItemsController"{
            // inicializace nového view controlleru a přetypování na tento typ controlleru
            let MixAddItemsVC = segue.destinationViewController as! MixAddItemsViewController
            // nastavení potřebných položek v novém controlleru
            MixAddItemsVC.mixID = self.mixID
            if ((thisItemRadios) != nil) {
                MixAddItemsVC.showPlaylists = false
            }
            else{
                MixAddItemsVC.showPlaylists = true
            }
        }
        else if segue.identifier == "goToMixSettings"{
            //předat hodnoty globálního nastavení do DetailMixSettingsViewController
            let selectedRow = self.tableView.indexPathForSelectedRow?.row
            // inicializace nového view controlleru a přetypování na tento typ controlleru
            let DetailMixSettingsVC = segue.destinationViewController as! DetailMixSettingsViewController
            // nastavení potřebných položek v novém controlleru
            DetailMixSettingsVC.mixID = self.mixID //id mixu
            if ((thisItemRadios) != nil) {
                DetailMixSettingsVC.mixType = 2 //typ mixu rádio streamů
            }
            else{
                DetailMixSettingsVC.mixType = 1 //typ mixu playlistů
            }
            if selectedRow > 0 {
                DetailMixSettingsVC.mixItemID = selectedRow!-1
            }
        }
    }
    

}
