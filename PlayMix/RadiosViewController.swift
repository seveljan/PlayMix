//
//  RadiosViewController.swift
//  PlayMix - třída pro zobrazení uložených a přidávání dalších radiových stramů
//
//  Created by Jan Sevela on 13/02/16.
//  Copyright © 2016 Jan Sevela. All rights reserved.
//

import UIKit
import AVFoundation


class RadiosViewController : UITableViewController {
    
    func addNewRadioStreamAction(sender: AnyObject) {
        let alert = createAlertController()
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("viewDidLoad-RadiosViewController")
        self.title = "Rádia(\(mainRadios.count))"
        let addRadioButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Add, target: self, action: #selector(RadiosViewController.addNewRadioStreamAction(_:)))
        self.navigationItem.rightBarButtonItem = addRadioButton
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        print("viewWillAppear-RadiosViewController")
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        print("viewWillDisappear-RadiosViewController")
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        print("viewDidAppear-RadiosViewController")
        self.tableView.reloadData()
    }
    
    /**
     Data tableview
     */
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return mainRadios.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("RadiosCell") as UITableViewCell!
        
        cell.textLabel?.text = mainRadios[indexPath.row].sectionName
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        print("vybranoRadio")
        //NSThread.sleepForTimeInterval(10)
        //performSegueWithIdentifier("goToPlayerViewController", sender: self) //složilo by pro načtení nového view controlleru, zde není třeba, už je dávno načteno
        self.playAndGoToPlayerVC(indexPath)
    }
    
    /**
     swipe akce nad položkou rádio streamu
     */
    override func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        let edit = UITableViewRowAction(style: .Normal, title: "Edit") { action, index in
            print("edit button tapped")
        }
        edit.backgroundColor = UIColor.lightGrayColor()
        
        let delete = UITableViewRowAction(style: .Destructive, title: "Delete") { action, index in
            print("delete button tapped")
            mainRadios.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        }
        delete.backgroundColor = UIColor.redColor()
        
        return [delete, edit]
    }
    
    /**
     Spuštění přehrávání rádio streamu
     */
    func playAndGoToPlayerVC(index: NSIndexPath?) {
        if let indexPath = index {
            gl.playingMode = 2
            // initializace PlayerViewControlleru
            let PlayerVC = self.tabBarController!.viewControllers![0] as! PlayerViewController
            
            PlayerVC.radio = radioMixItem(mixItem: mainRadios[indexPath.row], mixItemDuration: 0, mixItemPauseDuration: 0, mixItemRepeat: 0)
            gl.playingMode = 2 //gl.playingMode = režim radio streamu
            PlayerVC.playRadioWithUrl(mainRadios[indexPath.row].sectionURL, title: String(mainRadios[indexPath.row].sectionURL), artist: mainRadios[indexPath.row].sectionName)
            if(gl.avQueuePlayer.rate == 0){
                PlayerVC.play()
            }
            self.tabBarController?.selectedIndex = 0
        }
    }
    
    
    /**
     Allert controller pro přidání rádio streamu
     */
    func createAlertController() -> UIAlertController {
        
        let alert = UIAlertController(title: "Add radio", message: "Enter the values of adding radio stream!", preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addTextFieldWithConfigurationHandler { (text : UITextField) -> Void in
            text.placeholder = "Enter the name of radio"
        }
        
        alert.addTextFieldWithConfigurationHandler { (url : UITextField) -> Void in
            url.placeholder = "Enter the URL of radio stream"
        }

        
        let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) { (action :UIAlertAction) -> Void in
            
        }
        
        let ok = UIAlertAction(title: "Add", style: UIAlertActionStyle.Default) { _ in
            
            guard let textfields = alert.textFields, first = textfields.first else {
                return
            }
            
            let radionName = first.text!
            let radioUrl = textfields.last!.text!
            
            mainRadios.append(RadioItem(sectionName: radionName, sectionType: "Radio", sectionURL: NSURL(string: radioUrl)))
            
            self.tableView?.reloadData()
            print(mainRadios[0].sectionName)
            print(mainRadios[1].sectionName)
            print(mainRadios[2].sectionName)
            
        }
        alert.addAction(ok)
        alert.addAction(cancel)
        
        return alert
        
    }
    
}
