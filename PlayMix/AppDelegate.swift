//
//  AppDelegate.swift
//  PlayMix
//
//  Created by Jan Sevela on 11/02/16.
//  Copyright © 2016 Jan Sevela. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    
    
    override func remoteControlReceivedWithEvent(event: UIEvent?) {
        
        let tabBarVC = self.window!.rootViewController as! UITabBarController
        let vc = tabBarVC.viewControllers?.first as! PlayerViewController
        
        if event!.type == UIEventType.RemoteControl {
            //https://developer.apple.com/library/ios/documentation/UIKit/Reference/UIEvent_Class/
            
            if event!.subtype == UIEventSubtype.RemoteControlPlay {
                //print("received remote play")
                print("Control Center Play")
                vc.PlayingItemName.text = "Play"
                vc.PlayButtonTapped(vc.PlayingItemName)
                NSNotificationCenter.defaultCenter().postNotificationName("AudioPlayerIsPlaying", object: nil)
                
            } else if event!.subtype == UIEventSubtype.RemoteControlPause {
                //print("received remote pause")
                print ("Control Center Pause")
                vc.PlayingItemName.text = "Pause"
                vc.PlayPauseButton.sendActionsForControlEvents(UIControlEvents.TouchUpInside)
                NSNotificationCenter.defaultCenter().postNotificationName("AudioPlayerIsNotPlaying", object: nil)
                
            } else if event!.subtype == UIEventSubtype.RemoteControlNextTrack{
                //obdržení akce - další skladba
                //vc.nextBtn.sendActionsForControlEvents(UIControlEvents.TouchUpInside)
                
            }else if event!.subtype == UIEventSubtype.RemoteControlPreviousTrack{
                //obdržení akce - předchozí skladba
                //vc.backBtn.sendActionsForControlEvents(UIControlEvents.TouchUpInside)
            }
        }
    }


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

