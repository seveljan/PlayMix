//
//  MixesViewController.swift
//  PlayMix - třída pro zobrazení všech vytvořených mixů a pro vytvoření dalších playlistů
//
//  Created by Jan Sevela on 13/02/16.
//  Copyright © 2016 Jan Sevela. All rights reserved.
//

import UIKit

class MixesViewController : UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("viewDidLoad-MixesViewController")
        // Do any additional setup after loading the view, typically from a nib.
        self.title = "Mixy"
        let addMixButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Add, target: self, action: #selector(MixesViewController.addNewMixAction(_:)))
        self.navigationItem.rightBarButtonItem = addMixButton
        print("globalVariables.audioMixes.size(): "+String(gl.audioMixes.size()))
        print("globalVariables.radioMixes.size(): "+String(gl.radioMixes.size()))
        self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "MixesCell")
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        print("viewWillAppear-MixesViewController")
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        print("viewWillDisappear-MixesViewController")
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        print("viewDidAppear-MixesViewController")
        
    }
    
    /**
    Data do tableview
    */
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return gl.audioMixes.size()+gl.radioMixes.size() //Settings also
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("MixesCell"/*, forIndexPath:indexPath*/) as UITableViewCell!
        
        if (indexPath.row < gl.audioMixes.size()) {
            cell.textLabel!.text = gl.audioMixes.items[indexPath.row].mixName
            cell.textLabel!.text?.appendContentsOf(" (Playlisty)")
            print("audioMixes")
        }
        
        else {
            cell.textLabel?.text = gl.radioMixes.at(indexPath.row-gl.audioMixes.size()).mixName
            cell.textLabel?.text?.appendContentsOf(" (Rádia)")
            print("radioMixes")
        }
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        itemAction(self)
    }
    
    /**
     Akce - přidat nový mix
     */
    func addNewMixAction (sender: AnyObject) {
        let alert = createAlertController()
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    /**
     Alert controller pro vytvoření nového mixu
     */
    func createAlertController() -> UIAlertController{
        
        let alert = UIAlertController(title: "Přidat mix", message: "Zadej jméno pro mix a vytvoř jej jako specifikovaný typ!", preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addTextFieldWithConfigurationHandler { (text : UITextField) -> Void in
            text.placeholder = "zde napiš název mixu"
        }
        //Vytvoření mixu typu playlisty
        let option0 = UIAlertAction(title: "Vytvořit mix typu Playlist", style: UIAlertActionStyle.Default){ _ in
            
            guard let textfields = alert.textFields, first = textfields.first else {
                return
            }
            let mixName = first.text!
            let queue = Queue<audioMixItem>()
            gl.audioMixes.push(audioMix(mixName: mixName, mixQueue: queue, mixGlobalDuration: 0, mixGlobalPauseDuration: 0, mixGlobalItemRepeat: 0, mixGlobalSettingsON: true))
        
            self.tableView?.reloadData()
            print("add playlistMix - "+mixName)
        }
        //Vytvoření mixu typu rádio streamy
        let option1 = UIAlertAction(title: "Vytořit mix typu Rádio", style: UIAlertActionStyle.Default){ _ in
            
            guard let textfields = alert.textFields, first = textfields.first else {
                return
            }
            let mixName = first.text!
            let queue = Queue<radioMixItem>()
            gl.radioMixes.push(radioMix(mixName: mixName, mixQueue: queue, mixGlobalDuration: 0, mixGlobalPauseDuration: 0, mixGlobalItemRepeat: 0, mixGlobalSettingsON: true))
            
            self.tableView?.reloadData()
            print("add radioMix - "+mixName)
        }
        //Akce pro zrušení a neuložení ničeho
        let cancel = UIAlertAction(title: "Zrušit", style: UIAlertActionStyle.Cancel) { (action :UIAlertAction) -> Void in }
        
        alert.addAction(option0)
        alert.addAction(option1)
        alert.addAction(cancel)
        
        return alert
    }
    
    /**
     Akce - přidání vybaných položek do mixu
     */
    func itemAction (sender: AnyObject) {
        // Otevření alert, typu ActionSheet, dialogu s OK a Zrušit tlačítky
        let actionTitle: String
        actionTitle = NSLocalizedString("Chceš vybraný mix přehrát nebo editovat?", comment: "")
        
        let actionAlert = UIAlertController(title: actionTitle, message: nil, preferredStyle: .ActionSheet)
        actionAlert.popoverPresentationController?.sourceView = self.view
        
        let cancelTitle = NSLocalizedString("Zrušit", comment: "Zrušit pro akci.")
        let editTitle = NSLocalizedString("Editovat", comment: "Editovat položku mixů.")
        let playTitle = NSLocalizedString("Přehrát", comment: "Přehrávat položku mixů.")
        
        let cancelAction = UIAlertAction(title: cancelTitle, style: .Cancel) {_ in
            self.actionSheetClickedButtonAtIndex(0)
        }
        let editAction = UIAlertAction(title: editTitle, style: .Default) {_ in
            self.actionSheetClickedButtonAtIndex(1)
        }
        let playAction = UIAlertAction(title: playTitle, style: .Default) {_ in
            self.actionSheetClickedButtonAtIndex(2)
        }
        actionAlert.addAction(cancelAction)
        actionAlert.addAction(playAction)
        actionAlert.addAction(editAction)
        self.presentViewController(actionAlert, animated: true, completion: nil)
    }
    
    /**
     Akce - kliknutí na položky allertu typu ActionSheet
     
     - parameter buttonIndex: index kliknutého tlačítka
     */
    func actionSheetClickedButtonAtIndex(buttonIndex: Int) {
        if buttonIndex == 1 {
            // Přidání vybraných položek
            let indexPath = self.tableView.indexPathForSelectedRow
            
            if (indexPath!.row < gl.audioMixes.size()) {
                print("vybranPlaylistMix"+String(indexPath!.row))
                //NSThread.sleepForTimeInterval(10)
                performSegueWithIdentifier("goToMixItemsViewController", sender: self)
            }
            else {
                print("vybranRadiosMix"+String(indexPath!.row-gl.audioMixes.size()))
                //NSThread.sleepForTimeInterval(10)
                performSegueWithIdentifier("goToMixItemsViewController", sender: self)
            }
        }
        else if buttonIndex == 2 {
            // Přehrátí vybraných položek
            let indexPath = self.tableView.indexPathForSelectedRow
            
            if (indexPath!.row < gl.audioMixes.size()) {
                print("vybranPlaylistMix"+String(indexPath!.row))
                //NSThread.sleepForTimeInterval(10)
            }
            else {
                print("vybranRadiosMix"+String(indexPath!.row-gl.audioMixes.size()))
                //NSThread.sleepForTimeInterval(10)
            }
            if playAndGoToPlayerVC(indexPath) == false {
                self.tableView.deselectRowAtIndexPath(indexPath!, animated: true)
            }
        }
    }
    
    /**
     Příprava pro segue
     */
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        print("PrepareForSegue")
        if segue.identifier == "goToMixItemsViewController" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                if (indexPath.row < gl.audioMixes.size()) {
                    // initialize new view controller and cast it as your view controller
                    let MixItemsVC = segue.destinationViewController as! MixItemsViewController
                    // your new view controller should have property that will store passed value
                    MixItemsVC.thisItemPlaylists = gl.audioMixes.items[indexPath.row] as audioMix
                    MixItemsVC.mixID = indexPath.row
                }
                else {
                    // initialize new view controller and cast it as your view controller
                    let MixItemsVC = segue.destinationViewController as! MixItemsViewController
                    // your new view controller should have property that will store passed value
                    MixItemsVC.thisItemRadios = gl.radioMixes.items[indexPath.row-gl.audioMixes.size()] as radioMix
                    MixItemsVC.mixID = indexPath.row-gl.audioMixes.size()
                }
                
                print("Detail mixu cislo: "+String(indexPath.row))
            }
        }
    }
    
    /**
     Spuštění přehrávání vybraného mixu
     */
    func playAndGoToPlayerVC(index: NSIndexPath?) -> Bool{
        if let indexPath = index {
            // inicializace PlayerViewControlleru
            let PlayerVC = self.tabBarController!.viewControllers![0] as! PlayerViewController
            
            var letsGo = false
            
            //gl.playingMode = playlistsMix
            if (indexPath.row < gl.audioMixes.size()) {
                //je něco v mixu?
                if gl.audioMixes.items[indexPath.row].mixQueue.size() > 0{
                    letsGo = true
                    gl.playingMode = 3
                    gl.playlistsMix = gl.audioMixes.at(indexPath.row)// uložení do pomocné struktury mixu playlistů pro přehrávání
                    gl.playlistsMix!.mixGlobalItemRepeat = (gl.playlistsMix!.mixGlobalItemRepeat)*(gl.playlistsMix!.mixQueue.size()) //opakování musím vynásobit počtem položek mixu
                    let songsOfPlaylist = (gl.playlistsMix!.mixQueue.front().mixItem["songs"] as! NSArray).count
                    let songIndex = Int(arc4random_uniform(UInt32(songsOfPlaylist-1))) //nahodne cislo od 0 po velikost poctu skladeb z playlistu
                    //                NSThread.sleepForTimeInterval(5)
                    print("Spoustim skladbu z prvni polozky(playlistu) mixu: "+String(gl.playlistsMix?.mixQueue.front().mixItem["songs"]!.objectAtIndex(songIndex)))
                    let song = (gl.playlistsMix!.mixQueue.front().mixItem["songs"] as! NSArray).objectAtIndex(songIndex) as! NSDictionary
                    //                NSThread.sleepForTimeInterval(10)
                    PlayerVC.playlist = gl.playlistsMix!.mixQueue.front() //uložení prvního rádio streamu do pomocné struktury pro načítání metadat
                    PlayerVC.playSongWithId(song["songId"] as! NSNumber)
                    
                    if gl.playlistsMix?.mixGlobalSettingsON == true {
                        print("size bef repeat: "+String(gl.playlistsMix!.mixQueue.size()))
                        if(gl.playlistsMix!.mixGlobalItemRepeat > 0){
                            gl.playlistsMix!.mixGlobalItemRepeat = (gl.playlistsMix!.mixGlobalItemRepeat) - 1
                            gl.playlistsMix!.mixQueue.push((gl.playlistsMix!.mixQueue.front()))
                        }
                    }
                        
                    else{
                        if(PlayerVC.playlist!.mixItemRepeat > 0){
                            PlayerVC.playlist!.mixItemRepeat = (PlayerVC.playlist!.mixItemRepeat) - 1
                            gl.playlistsMix!.mixQueue.push(PlayerVC.playlist!)
                        }
                    }
                    print("size bef pop: "+String(gl.playlistsMix!.mixQueue.size()))
                    gl.playlistsMix!.mixQueue.pop()
                    print("size aft pop: "+String(gl.playlistsMix!.mixQueue.size()))
                }
            }
            else {
                //je něco v mixu
                if gl.radioMixes.items[indexPath.row-gl.audioMixes.size()].mixQueue.size() > 0{
                    letsGo = true
                    gl.playingMode = 4
                    gl.radiosMix = gl.radioMixes.at(indexPath.row-gl.audioMixes.size())// uložení do pomocné struktury mixu rádio streamů pro přehrávání
                    gl.radiosMix!.mixGlobalItemRepeat = (gl.radioMixes.at(indexPath.row-gl.audioMixes.size()).mixGlobalItemRepeat)*(gl.radiosMix!.mixQueue.size()) //opakování musím vynásobit počtem položek mixu
                    PlayerVC.radio = gl.radiosMix?.mixQueue.front() //uložení prvního rádio streamu do pomocné struktury pro načítání metadat
                    print("Spoustim první rádio stream z mixu: "+String(PlayerVC.radio!.mixItem.sectionName))
                    PlayerVC.playRadioWithUrl(PlayerVC.radio!.mixItem.sectionURL,title: String(PlayerVC.radio!.mixItem.sectionURL),artist: String(PlayerVC.radio!.mixItem.sectionName))
                    
                    if gl.radiosMix?.mixGlobalSettingsON == true {
                        print("size bef repeat: "+String(gl.radiosMix?.mixQueue.size()))
                        if(gl.radiosMix?.mixGlobalItemRepeat > 0){
                            gl.radiosMix?.mixGlobalItemRepeat = (gl.radiosMix?.mixGlobalItemRepeat)! - 1
                            gl.radiosMix?.mixQueue.push((gl.radiosMix?.mixQueue.front())!)
                        }
                    }
                        
                    else{
                        if(PlayerVC.radio!.mixItemRepeat > 0){
                            PlayerVC.radio!.mixItemRepeat = (PlayerVC.radio!.mixItemRepeat) - 1
                            gl.radiosMix?.mixQueue.push(PlayerVC.radio!)
                        }
                    }
                    print("size bef pop: "+String(gl.radiosMix?.mixQueue.size()))
                    gl.radiosMix!.mixQueue.pop()
                    print("size aft pop: "+String(gl.radiosMix?.mixQueue.size()))
                }
            }
            
            if letsGo == true {
                //player nejede, tak jej spustím
                if(gl.avQueuePlayer.rate == 0){
                    PlayerVC.play()
                }
                //přepnutí na kontroler přehrávače
                self.tabBarController?.selectedIndex = 0
                return true
            }
        }
        return false
    }
}