//
//  ViewController.swift
//  PlayMix - Třída pro hlavní přehrávač
//
//  Created by Jan Sevela on 11/02/16.
//  Copyright © 2016 Jan Sevela. All rights reserved.
//

//import Foundation
import UIKit
import MediaPlayer

/**
 Třída hlavního přehrávače, která se stará o přehrávání 4 možných módů, 1=skladba z playlistu, 2=stream rádia ze seznamu streamů, 3=mix typu playlisty, 4=mix typu rádiové streamy
 */
class PlayerViewController: UIViewController {
    
    var radio:radioMixItem?                              //pomocný radio prvek, pro aktualizaci názvu streamu
    var playlist:audioMixItem?                           //pomocný radio prvek, pro aktualizaci názvu streamu
    
    var trackTime: Float64 = 0.0                         //pomocný prvek pro výpočet a zobrazení času přehrávání
    var durationTime: Float64 = 0.0                      //pomocný prvek pro výpočet a zobrazení zbývajícího času skladby
    var progress: Float64 = 0.0                          //pomocný prvek pro výpočet a vizuální zobrazení rodílu mezi aktuální dobou přehrávání a zbývajícím časem
    
    /**
     Informace o přehrávané položce - textové popisky
     */
    
    @IBOutlet weak var PlayingItemName: UILabel!
    @IBOutlet weak var PlayingItemArtist: UILabel!
    
    /**
     Informace o přehrávané položce - progres
     */
    @IBOutlet weak var DurationSlider: UISlider!
    
    /**
     Posun písničky na nějaký čas
     */
    @IBAction func ShiftCurrentTime(sender: AnyObject) {
        gl.avQueuePlayer.currentItem?.seekToTime(CMTimeMakeWithSeconds(Float64(DurationSlider.value)*durationTime,1))
        gl.avQueuePlayer.volume = gl.volumeBefore
    }
    
    /**
     Informace o přehrávané položce - aktuální čas
     */
    @IBOutlet weak var LabelTrackTime: UILabel!
    /**
     Informace o přehrávané položce - délka
     */
    @IBOutlet weak var LabelTrackDuration: UILabel!
    
    /**
    Play/Pause tlačítko + akce tohoto tlačítka (spuštění, pokud nic nehraje a zároveň je co pouštět, pauza pokud je co pauzovat)
    */
    @IBOutlet weak var PlayPauseButton: UIButton!
    @IBAction func PlayButtonTapped(sender: AnyObject) {
        if gl.avQueuePlayer.rate == 0 && gl.avQueuePlayer.currentItem != nil {
            print("poustim")
            self.play()
        }
        else if gl.avQueuePlayer.currentItem != nil {
            print("pauzuji")
            self.pause()
        }
    }
    
    /**
    Posuvník hlasitosti + akce tohoto posuvníku (pokud se posune, tak se změní hlasitost přehrávače)
    */
    @IBOutlet weak var VolumeSlider: UISlider!
    @IBAction func VolumeSliderStatus(sender: AnyObject) {
        gl.volumeBefore = gl.avQueuePlayer.volume
        gl.avQueuePlayer.volume = self.VolumeSlider.value
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.initAudioSession() //inicializace audia, na pozadí a jíné vlastnosti
        gl.avQueuePlayer.actionAtItemEnd = .Advance //akce po ukončení skladby, začne hrád další ve frontě, zatím nevyužito, kvůli vlastní frontě
        gl.volumeBefore = gl.avQueuePlayer.volume
        self.VolumeSlider.value = gl.avQueuePlayer.volume //nastavení posuvníku hlasitosti podle hlasitosti přehrávače
        
        print("viewDidLoad-PlayerViewController")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        print("viewWillAppear-PlayerViewController")
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        print("viewWillDisappear-PlayerViewController")
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        print("viewDidAppear-PlayerViewController")

        if gl.avQueuePlayer.currentItem != nil {
            print("fronta neni prazdna, aktualizuji informace o tom co hraje")
            self.loadItemInfo()
        }
        else {
            print("neni co prehravat, fronta prazdna")
        }
        
        if(gl.playingMode == 1 || gl.playingMode == 3){ //zobrazení prvků podle módu přehrávání (1=skladba z playlistu, 3=mix typu playlisty)
            DurationSlider.hidden = false
            LabelTrackTime.hidden = false
            LabelTrackDuration.hidden = false
        }
        else if gl.playingMode == 4 { //zobrazení prvků podle módu přehrávání (2=stream rádia ze seznamu streamů, 4=mix typu rádiové streamy)
            if gl.radiosMix?.mixGlobalSettingsON == true {
                if gl.radiosMix?.mixGlobalDuration > 0 {
                    DurationSlider.hidden = false
                    LabelTrackDuration.hidden = false
                }
                else{
                    DurationSlider.hidden = true
                    LabelTrackDuration.hidden = true
                }
            }
            else{
                if radio?.mixItemDuration > 0 {
                    DurationSlider.hidden = false
                    LabelTrackDuration.hidden = false
                }
                else{
                DurationSlider.hidden = true
                LabelTrackDuration.hidden = true
                }
            }
            LabelTrackTime.hidden = false
        }
        else{
            DurationSlider.hidden = true
            LabelTrackTime.hidden = false
            LabelTrackDuration.hidden = true
        }
        
        //naslouchání vzdálených akcí Control centra
        UIApplication.sharedApplication().beginReceivingRemoteControlEvents()
        becomeFirstResponder()
    }
    
    /**
    Metoda pro spuštění přehrávání
    */
    func play() {
        gl.timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target:self, selector: #selector(PlayerViewController.playControl), userInfo: nil, repeats: true) //nastavení timeru a kontrolní
        gl.avQueuePlayer.play()
        self.PlayPauseButton.setImage(UIImage(named: "pauseButton"),forState: UIControlState.Normal)
        self.loadItemInfo()
        //self.VolumeSlider.value = gl.avQueuePlayer.volume
        self.VolumeSliderStatus(self)
        print("play()")
        //pausedStatus = false
    }
    
    /**
    Metoda pro pauzu
    */
    func pause() {
        gl.avQueuePlayer.pause()
        self.PlayPauseButton.setImage(UIImage(named: "playButton"),forState: UIControlState.Normal)
        self.loadItemInfo()
        print("pause()")
        gl.timer?.invalidate()
    }
    
    func setTrackTimeNow(tmpTrackTime: Float64){
        trackTime = tmpTrackTime
        LabelTrackTime.text = String(format: "%02d:%02d", ((lround(trackTime) / 60) % 60), lround(trackTime) % 60)
    }
    
    func setDurationAndProgressNow(tmpDurationTime: Float64){
        durationTime = tmpDurationTime
        LabelTrackDuration.text = String(format: "%02d:%02d", ((lround(durationTime-trackTime) / 60) % 60), lround(durationTime-trackTime) % 60)
        
        progress = trackTime / durationTime
        DurationSlider.setValue(Float(progress), animated: false)
    }
    
    /**
     Metoda pro kontrolu přehrávané položky a zobrazení aktuálních informací o ní (dobra trvání, progres, přepnutí na další z mixu apod.)
     */
    func playControl(){
        if(gl.avQueuePlayer.currentItem != nil){ //jen pokud je něco
            
            if(gl.playingMode == 1 || gl.playingMode == 2){
                
                //nastavení času trvání
                setTrackTimeNow((gl.avQueuePlayer.currentItem?.currentTime().seconds)!)
                
                if gl.playingMode == 1{
                    
                    //nastavení zbývajícího času a progress slideru
                    setDurationAndProgressNow((gl.avQueuePlayer.currentItem?.duration.seconds)!)
                }
            }
            
            //přehrávání mixů typu radio stream
            if gl.playingMode == 4 && gl.avQueuePlayer.rate > 0 {
                
                //nastavení času trvání
                setTrackTimeNow((gl.avQueuePlayer.currentItem?.currentTime().seconds)!)
                
                //fronta položek přehrávaného mixu je neprázdná
                if(gl.radiosMix!.mixQueue.size() > 0){

                    //globální nastavení mixu zapnuto
                    if gl.radiosMix?.mixGlobalSettingsON == true {
                        
                        if(gl.radiosMix?.mixGlobalDuration > 0){
                            
                            //nastavení zbývajícího času a progress slideru
                            setDurationAndProgressNow((gl.radiosMix?.mixGlobalDuration)!)
                        }
                        
                        //ztlumování posledních 5 sekund
                        if gl.radiosMix?.mixGlobalDuration != 0 && (trackTime > (gl.radiosMix?.mixGlobalDuration)!-5){
                            self.volumeFade()
                        }
                        
                        //čas přesáhl globální nastavení pložky mixu
                        if gl.radiosMix?.mixGlobalDuration != 0 && trackTime > gl.radiosMix?.mixGlobalDuration {
                            radio = gl.radiosMix?.mixQueue.front()
                            print("Prepinam na: "+String(radio?.mixItem.sectionName))
                            self.pause()
                            NSThread.sleepForTimeInterval(NSTimeInterval(gl.radiosMix!.mixGlobalPauseDuration))
                            playRadioWithUrl(radio!.mixItem.sectionURL,title: String(radio!.mixItem.sectionURL),artist: String(radio!.mixItem.sectionName))
                            gl.avQueuePlayer.volume = gl.volumeBefore
                            self.play()
                            
                            print("size bef repeat: "+String(gl.radiosMix?.mixQueue.size()))
                            if(gl.radiosMix?.mixGlobalItemRepeat > 0){
                                gl.radiosMix?.mixGlobalItemRepeat = (gl.radiosMix?.mixGlobalItemRepeat)! - 1
                                gl.radiosMix?.mixQueue.push((gl.radiosMix?.mixQueue.front())!)
                            }
                            print("size bef pop: "+String(gl.radiosMix?.mixQueue.size()))
                            gl.radiosMix!.mixQueue.pop()
                            print("size aft pop: "+String(gl.radiosMix?.mixQueue.size()))
                        }
                    }
                    //globální nastavení mixu vypnuto
                    else {
                        
                        if(radio?.mixItemDuration > 0){
                            
                            //nastavení zbývajícího času a progress slideru
                            setDurationAndProgressNow((gl.radiosMix?.mixGlobalDuration)!)
                        }
                        
                        //ztlumování posledních 5 sekund
                        if radio?.mixItemDuration != 0 && (trackTime > (radio?.mixItemDuration)!-5){
                            self.volumeFade()
                        }
                        
                        //čas přesáhl lokální nastavení pložky mixu
                        if radio?.mixItemDuration != 0 && trackTime > radio!.mixItemDuration {
                            print("lokal, prepinam...")
                            radio = gl.radiosMix!.mixQueue.front()
                            self.pause()
                            NSThread.sleepForTimeInterval(NSTimeInterval(radio!.mixItemPauseDuration))
                            self.playRadioWithUrl(radio!.mixItem.sectionURL,title: String(radio!.mixItem.sectionURL),artist: String(radio!.mixItem.sectionName))
                            gl.avQueuePlayer.volume = gl.volumeBefore
                            self.play()
                            
                            print("size bef repeat: "+String(gl.radiosMix?.mixQueue.size()))
                            print("repeat: "+String(radio!.mixItemRepeat))
                            if(radio!.mixItemRepeat > 0){
                                radio!.mixItemRepeat = (radio!.mixItemRepeat) - 1
                                gl.radiosMix?.mixQueue.push(radio!)
                            }
                            print("size bef pop: "+String(gl.radiosMix?.mixQueue.size()))
                            gl.radiosMix!.mixQueue.pop()
                            print("size aft pop: "+String(gl.radiosMix?.mixQueue.size()))
                        }
                    }
                }
                //už není žádná položka v mixu
                else{
                    //globální nastavení mixu zapnuto
                    if gl.radiosMix?.mixGlobalSettingsON == true {
                        
                        if(gl.radiosMix?.mixGlobalDuration > 0){
                            
                            //nastavení zbývajícího času a progress slideru
                            setDurationAndProgressNow((gl.radiosMix?.mixGlobalDuration)!)
                        }
                        
                        //ztlumování posledních 5 sekund
                        if gl.radiosMix?.mixGlobalDuration != 0 && (trackTime > (gl.radiosMix?.mixGlobalDuration)!-5){
                            self.volumeFade()
                        }
                        
                        //čas přesáhl globální nastavení pložky mixu
                        if gl.radiosMix?.mixGlobalDuration != 0 && trackTime > gl.radiosMix?.mixGlobalDuration {
                            self.pause()
                            gl.avQueuePlayer.advanceToNextItem()
                            gl.avQueuePlayer.volume = gl.volumeBefore
                            //gl.volumeBefore = 0
                        }
                    }
                    //globální nastavení mixu vypnuto
                    else{
                        
                        if(radio?.mixItemDuration > 0){
                            
                            //nastavení zbývajícího času a progress slideru
                            setDurationAndProgressNow((radio?.mixItemDuration)!)
                        }
                        
                        //ztlumování posledních 3 sekund
                        if radio?.mixItemDuration != 0 && (trackTime > (radio!.mixItemDuration)-5){
                            self.volumeFade()
                        }
                        
                        //čas přesáhl lokální nastavení položky mixu
                        if radio?.mixItemDuration != 0 && trackTime > radio!.mixItemDuration {
                            self.pause()
                            gl.avQueuePlayer.advanceToNextItem()
                            gl.avQueuePlayer.volume = gl.volumeBefore
                            //gl.volumeBefore = 0
                        }
                    }
                }
            }
            //přehrávání mixů typu položky playlistů
            else if gl.playingMode == 3 {
                
                //nastavení času trvání
                setTrackTimeNow((gl.avQueuePlayer.currentItem?.currentTime().seconds)!)
                
                //fronta položek přehrávaného mixu je neprázdná
                if(gl.playlistsMix!.mixQueue.size() > 0){
                    //globální nastavení mixu zapnuto
                    if gl.playlistsMix?.mixGlobalSettingsON == true {
                        
                        if(gl.playlistsMix?.mixGlobalDuration == 0 || gl.playlistsMix?.mixGlobalDuration > gl.avQueuePlayer.currentItem!.duration.seconds){
                            
                            //nastavení zbývajícího času a progress slideru
                            setDurationAndProgressNow(gl.avQueuePlayer.currentItem!.duration.seconds)
                        }
                        
                        else{
                            //nastavení zbývajícího času a progress slideru
                            setDurationAndProgressNow((gl.playlistsMix?.mixGlobalDuration)!)
                        }
                        
                        //ztlumování posledních 5 sekund pro globalní čas trvání rovno 0 nebo větší než délka skladby
                        if ((gl.playlistsMix?.mixGlobalDuration == 0 || gl.playlistsMix?.mixGlobalDuration > gl.avQueuePlayer.currentItem!.duration.seconds) && trackTime > gl.avQueuePlayer.currentItem!.duration.seconds-5 ){
                            self.volumeFade()
                        }
                        //ztlumování posledních 5 sekund
                        else if gl.playlistsMix?.mixGlobalDuration != 0 && (trackTime > (gl.playlistsMix?.mixGlobalDuration)!-5){
                            self.volumeFade()
                        }
                        
                        //PŘEPÍNÁNÍ - čas přesáhl globální nastavení pložky mixu
                        if (gl.playlistsMix?.mixGlobalDuration != 0 && trackTime > gl.playlistsMix?.mixGlobalDuration) ||
                            trackTime > gl.avQueuePlayer.currentItem!.duration.seconds-0.1
                        {
                            playlist = gl.playlistsMix?.mixQueue.front() //uložení playlistu do pomocné struktury pro možnost změny repeat parametru
                            
                            let songsOfPlaylist = (playlist!.mixItem["songs"] as! NSArray).count
                            let songIndex = Int(arc4random_uniform(UInt32(songsOfPlaylist-1)))//nahodne cislo od 0 po velikost poctu skladeb z playlistu
                            let song = (playlist!.mixItem["songs"] as! NSArray).objectAtIndex(songIndex) as! NSDictionary
                            
                            self.pause()
                            NSLog("Prepinam na skladbu dlasi polozky(playlistu) mixu: "+String(playlist!.mixItem["songs"]!.objectAtIndex(songIndex)))
                            NSThread.sleepForTimeInterval(NSTimeInterval(gl.playlistsMix!.mixGlobalPauseDuration)) //obslouzim pauzu
                            playSongWithId(song["songId"] as! NSNumber)
                            gl.avQueuePlayer.volume = gl.volumeBefore
                            //gl.volumeBefore = 0
                            self.play()
                            
                            NSLog("size bef repeat: "+String(gl.playlistsMix?.mixQueue.size()))
                            if(gl.playlistsMix?.mixGlobalItemRepeat > 0){
                                gl.playlistsMix?.mixGlobalItemRepeat = (gl.playlistsMix?.mixGlobalItemRepeat)! - 1
                                gl.playlistsMix?.mixQueue.push((gl.playlistsMix?.mixQueue.front())!)
                            }
                            NSLog("size bef pop: "+String(gl.playlistsMix?.mixQueue.size()))
                            gl.playlistsMix!.mixQueue.pop()
                            NSLog("size aft pop: "+String(gl.playlistsMix?.mixQueue.size()))
                        }
                    }
                    //globální nastavení mixu vypnuto
                    else {
                        
                        if(playlist?.mixItemDuration == 0 || playlist?.mixItemDuration > gl.avQueuePlayer.currentItem!.duration.seconds){
                            //nastavení zbývajícího času a progress slideru
                            setDurationAndProgressNow(gl.avQueuePlayer.currentItem!.duration.seconds)
                        }
                        
                        else{
                            //nastavení zbývajícího času a progress slideru
                            setDurationAndProgressNow((playlist?.mixItemDuration)!)
                        }
                        
                        //ztlumování posledních 5 sekund pro lokální čas trvání rovno 0 nebo větší než délka skladby
                        if((playlist?.mixItemDuration == 0 || playlist?.mixItemDuration > gl.avQueuePlayer.currentItem!.duration.seconds) && trackTime > gl.avQueuePlayer.currentItem!.duration.seconds-5 ){
                            self.volumeFade()
                        }
                        //ztlumování posledních 5 sekund
                        else if playlist?.mixItemDuration != 0 && (trackTime > (playlist?.mixItemDuration)!-5){
                            self.volumeFade()
                        }
                        
                        //PŘEPÍNÁNÍ - čas přesáhl lokální nastavení pložky mixu
                        if (playlist!.mixItemDuration != 0 && trackTime > playlist!.mixItemDuration) || trackTime > gl.avQueuePlayer.currentItem!.duration.seconds-0.1 {
                            NSLog("lokal, prepinam...")
                            playlist = gl.playlistsMix!.mixQueue.front() //uložení playlistu do pomocné struktury pro možnost změny repeat parametru
                            
                            let songsOfPlaylist = (playlist!.mixItem["songs"] as! NSArray).count
                            let songIndex = Int(arc4random_uniform(UInt32(songsOfPlaylist-1)))//nahodne cislo od 0 po velikost poctu skladeb z playlistu
                            let song = (playlist!.mixItem["songs"] as! NSArray).objectAtIndex(songIndex) as! NSDictionary
                            
                            self.pause()
                            NSLog("Prepinam na skladbu dlasi polozky(playlistu) mixu: "+String(playlist!.mixItem["songs"]!.objectAtIndex(songIndex)))
                            NSThread.sleepForTimeInterval(NSTimeInterval(playlist!.mixItemPauseDuration))
                            playSongWithId(song["songId"] as! NSNumber)
                            gl.avQueuePlayer.volume = gl.volumeBefore
                            //gl.volumeBefore = 0
                            self.play()
                            
                            NSLog("size bef repeat: "+String(gl.playlistsMix?.mixQueue.size()))
                            if(playlist!.mixItemRepeat > 0){
                                playlist!.mixItemRepeat = (playlist!.mixItemRepeat) - 1
                                gl.playlistsMix?.mixQueue.push(playlist!)
                            }
                            NSLog("size bef pop: "+String(gl.playlistsMix?.mixQueue.size()))
                            gl.playlistsMix!.mixQueue.pop()
                            NSLog("size aft pop: "+String(gl.playlistsMix?.mixQueue.size()))
                        }
                    }
                }
                //už není žádná položka v mixu
                else{
                    //globální nastavení mixu zapnuto
                    if gl.playlistsMix?.mixGlobalSettingsON == true {
                        
                        if(gl.playlistsMix?.mixGlobalDuration == 0 || gl.playlistsMix?.mixGlobalDuration > gl.avQueuePlayer.currentItem!.duration.seconds){
                            
                            //nastavení zbývajícího času a progress slideru
                            setDurationAndProgressNow(gl.avQueuePlayer.currentItem!.duration.seconds)
                        }
                        else{
                            //nastavení zbývajícího času a progress slideru
                            setDurationAndProgressNow((gl.playlistsMix?.mixGlobalDuration)!)
                        }
                        
                        if(gl.avQueuePlayer.rate > 0){
                            //ztlumování posledních 5 sekund pro globalní čas trvání rovno 0 nebo větší než délka skladby
                            if ((gl.playlistsMix?.mixGlobalDuration == 0 || gl.playlistsMix?.mixGlobalDuration > gl.avQueuePlayer.currentItem!.duration.seconds) && trackTime > gl.avQueuePlayer.currentItem!.duration.seconds-5 ){
                                self.volumeFade()
                            }
                            //ztlumování posledních 5 sekund
                            else if gl.playlistsMix?.mixGlobalDuration != 0 && (trackTime > (gl.playlistsMix?.mixGlobalDuration)!-5){
                                self.volumeFade()
                            }
                        }
                        
                        //PŘEPÍNÁNÍ - čas přesáhl globální nastavení pložky mixu
                        if (gl.playlistsMix?.mixGlobalDuration != 0 && trackTime > gl.playlistsMix?.mixGlobalDuration) || trackTime > gl.avQueuePlayer.currentItem!.duration.seconds-0.1 {
                            self.pause()
                            gl.avQueuePlayer.advanceToNextItem()
                            gl.avQueuePlayer.volume = gl.volumeBefore
                            //gl.volumeBefore = 0
                        }
                    }
                    //globální nastavení mixu vypnuto
                    else{
                        
                        if(playlist?.mixItemDuration == 0 || playlist?.mixItemDuration > gl.avQueuePlayer.currentItem!.duration.seconds){
                            
                            //nastavení zbývajícího času a progress slideru
                            setDurationAndProgressNow(gl.avQueuePlayer.currentItem!.duration.seconds)
                        }
                        else{
                            //nastavení zbývajícího času a progress slideru
                            setDurationAndProgressNow((playlist?.mixItemDuration)!)
                        }
                        
                        if(gl.avQueuePlayer.rate > 0){
                            //ztlumování posledních 5 sekund pro lokální čas trvání rovno 0 nebo větší než délka skladby
                            if((playlist?.mixItemDuration == 0 || playlist?.mixItemDuration > gl.avQueuePlayer.currentItem!.duration.seconds) && trackTime > gl.avQueuePlayer.currentItem!.duration.seconds-5 ){
                                self.volumeFade()
                            }
                                //ztlumování posledních 5 sekund
                            else if playlist?.mixItemDuration != 0 && (trackTime > (playlist?.mixItemDuration)!-5){
                                self.volumeFade()
                            }
                        }
                        
                        //PŘEPÍNÁNÍ - čas přesáhl lokální nastavení položky mixu
                        if (playlist!.mixItemDuration != 0 && trackTime > playlist!.mixItemDuration) || trackTime > gl.avQueuePlayer.currentItem!.duration.seconds-1 {
                            self.pause()
                            gl.avQueuePlayer.advanceToNextItem()
                            gl.avQueuePlayer.volume = gl.volumeBefore
                            //gl.volumeBefore = 0
                        }
                    }
                }
            }
        }
    }
    
    /**
    Načítání metadat položky
    */
    func loadItemInfo() {
        
        let typeOfItem = gl.avQueuePlayer.currentItem?.description
        print ("popis \n"+typeOfItem!)
        
        var itemName: String?
        var itemArtist: String?
        
        if(gl.playingMode == 1 || gl.playingMode == 3){ //skladba z playlistů
            let playerItem = gl.avQueuePlayer.currentItem  //toto je zdroj metadat
            let metadataList = playerItem!.asset.metadata
            for item in metadataList {
                if item.commonKey != nil && item.value != nil { // iTunes audio položky
                    print("item.commonKey")
                    if item.commonKey  == "title" {
                        print(item.stringValue)
                        itemName = item.stringValue!
                    }
//                    if item.commonKey  == "type" {
//                        print(item.stringValue)
//                    }
//                    if item.commonKey  == "albumName" {
//                        print(item.stringValue)
//                    }
                    if item.commonKey   == "artist" {
                        print(item.stringValue)
                        itemArtist = item.stringValue!
                    }
//                    if item.commonKey  == "artwork" {
//                        if let image = UIImage(data: item.value as! NSData) {
//                            print(image.description)
//                        }
//                    }
                }
            }
        }
        else { // radio stream
            itemName = String (gl.avQueuePlayer.currentItem?.asset.valueForKey("URL") as! NSURL)
            //print(self.avQueuePlayer.currentItem?.asset.valueForKey("URL"))
            itemArtist = radio!.mixItem.sectionName
        }
        //display now playing info on control center
        PlayingItemName.text = itemName
        PlayingItemArtist.text = itemArtist
        MPNowPlayingInfoCenter.defaultCenter().nowPlayingInfo = [MPMediaItemPropertyTitle: itemName!, MPMediaItemPropertyArtist: itemArtist!]
    }
    
    /**
     Postupné ztlumení
     */
    func volumeFade(){
        if gl.avQueuePlayer.volume > 0 {
            if(gl.volumeBefore == 0){
                gl.volumeBefore = gl.avQueuePlayer.volume
            }
            gl.avQueuePlayer.volume -= 1/50
        }
    }
    
    /**
    Inicializace audio session
     */
    func initAudioSession() {
        
//        NSNotificationCenter.defaultCenter().addObserver(self, selector: "audioSessionInterrupted:", name: AVAudioSessionInterruptionNotification, object: AVAudioSession.sharedInstance())
        var error:NSError?
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
        } catch let error1 as NSError {
            error = error1
        }
        
        if let _ = error {
            print("an error occurred when audio session category.\n \(error)")
        }
        
        var activationError:NSError?
        let success: Bool
        do {
            try AVAudioSession.sharedInstance().setActive(true)
            success = true
        } catch let error as NSError {
            activationError = error
            success = false
        }
        if !success {
            if let nonNilActivationError = activationError {
                print("an error occurred when audio session category.\n \(nonNilActivationError)")
            } else {
                print("audio session could not be activated")
            }
        }
    }
    
    /**
    Metoda pro spuštění přehrávání položky s URL - nesmím zde nic měnit self, kvůli prepareforsegue
     
     - parameter url: url rádio streamu
     - parameter title: název rádio streamu
     - parameter artist: autor rádio streamu
     
    */
    func playRadioWithUrl(url:NSURL, title:String, artist:String) {
        let assetUrl:NSURL = url
        let avSongItem = AVPlayerItem(URL: assetUrl)
        
        gl.avQueuePlayer.insertItem(avSongItem, afterItem: gl.avQueuePlayer.currentItem)
        if (gl.avQueuePlayer.items().count > 1){
            gl.avQueuePlayer.advanceToNextItem()
        }
        print("přidal jsem do fronty položku rádio stream a přepl na ni:")
        print(url)
        print(title)
        print(artist)
    }
    
    /**
    Metoda pro spuštění přehrávání položky s ID ze zařízení - nelze zde nic měnit self, kvůli prepareforsegue, apliakace by spadla
     
     - parameter songId: id skladby
    */
    func playSongWithId(songId:NSNumber) {
        MusicQuery().queryForSongWithId(songId, completionHandler: {[] (result:MPMediaItem?) -> Void in
            if let nonNilResult = result {
                let assetUrl:NSURL = nonNilResult.valueForProperty(MPMediaItemPropertyAssetURL) as! NSURL
                let avSongItem = AVPlayerItem(URL: assetUrl)
                gl.avQueuePlayer.insertItem(avSongItem, afterItem: gl.avQueuePlayer.currentItem)
                if (gl.avQueuePlayer.items().count > 1){
                    gl.avQueuePlayer.advanceToNextItem()
                }
            }
        })
    }
}

