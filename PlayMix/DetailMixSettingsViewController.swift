//
//  DetailMixSettingsViewController.swift
//  PlayMix - třída pro zobrazení globálního nastavení mixu a nebo lokální nastavení položky z mixu 
//
//  Created by Jan Sevela on 30/04/16.
//  Copyright © 2016 Jan Sevela. All rights reserved.
//

import UIKit
import Foundation

class DetailMixSettingsViewController : UIViewController,UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate {
    
    var mixID: Int! //pomocná proměná pro zachování informace o ID mixu
    var mixItemID: Int! //pomocná pro měnná pro zachování informace o ID prvku z mixu
    var mixType: Int! //pomocná proměnná pro uložení informace o jeký mix jde
    
    var detailDurationHours: Float64 = 0.0 //pomocná proměnná pro uložení informace o hodinách v době trvání
    var detailDurationMin: Float64 = 0.0 //pomocná proměnná pro uložení informace o minutách v době trvání
    var detailDurationSec: Float64 = 0.0 //pomocná proměnná pro uložení informace o sekundách v době trvání
    var detailPauseDurationHours: Float64 = 0.0 //pomocná proměnná pro uložení informace o hodinách v době pauze
    var detailPauseDurationMin: Float64 = 0.0 //pomocná proměnná pro uložení informace o minutách v době pauze
    var detailPauseDurationSec: Float64 = 0.0 //pomocná proměnná pro uložení informace o sekundách v době pauze
    var detailRepeat: Int = 0 //pomocná proměnná pro uložení počtu opakování
    
    var textFieldSelected:Int = 0 //pomocná proměnná pro uložení informace jaký textField je aktivní
    var repeatArray: [Int] = [] //pomocné pole pro hodnoty, které může nabývat opakování
    var hoursArray:[Int] = [] //pomocné pole pro hodnoty, které nabývají pro jednotky hodin
    var minsecArray:[Int] = [] //pomocné pole pro hodnoty, které nabývají pro jednotky minut a sekund
    
    
    @IBOutlet weak var globalSettingsSwitch: UISwitch!
    @IBOutlet weak var durationTextField: UITextField!
    @IBOutlet weak var pauseTextField: UITextField!
    @IBOutlet weak var repeatTextField: UITextField!
    @IBOutlet weak var settingsPicker: UIPickerView!
    
    @IBAction func globalSettingsSwitchChanged(sender: AnyObject) {
        self.updateView()
        self.activeTextField(0)
        self.settingsPicker.hidden = true
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("viewDidLoad-DetailMixSettingsViewController")
        // Do any additional setup after loading the view, typically from a nib.
        
        self.settingsPicker.hidden = true
        self.settingsPicker.delegate = self
        self.settingsPicker.dataSource = self
        self.durationTextField.delegate = self
        self.pauseTextField.delegate = self
        self.repeatTextField.delegate = self
        
        for r in 0...99 {
            self.repeatArray.append(r)
        }
        for h in 0...23 {
            self.hoursArray.append(h)
        }
        for m in 0...59 {
            self.minsecArray.append(m)
        }
        self.getValues()
        self.updateView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        print("viewWillAppear-DetailMixSettingsViewController")
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        print("viewWillDisappear-DetailMixSettingsViewController")
        self.setValues()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        print("viewDidAppear-DetailMixSettingsViewController")
        
    }
    
    /**
     Data pickerview
     */
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        if textFieldSelected != 3 {
            return 3
        }
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if textFieldSelected != 3 {
            if component == 0{
                return self.hoursArray.count
            }
            else {
                return self.minsecArray.count
            }
        }
        return self.repeatArray.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if textFieldSelected != 3 {
            if component == 0{
                return String(hoursArray[row])
            }
            else {
                return String(minsecArray[row])
            }
        }
        return String(repeatArray[row])
    }
    

    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if textFieldSelected == 1 {
            if component == 0{
                self.detailDurationHours = Float64(self.hoursArray[row])
            }
            else if component == 1{
                self.detailDurationMin = Float64(self.minsecArray[row])
            }
            else {
                self.detailDurationSec = Float64(self.minsecArray[row])
            }
        }
        else if textFieldSelected == 2{
            if component == 0{
                self.detailPauseDurationHours = Float64(self.hoursArray[row])
            }
            else if component == 1{
                self.detailPauseDurationMin = Float64(self.minsecArray[row])
            }
            else {
                self.detailPauseDurationSec = Float64(self.minsecArray[row])
            }
        }
        else{
            self.detailRepeat = Int(self.repeatArray[row])
        }
        updateTextFields()
    }
    
    /**
     Akce po kliknutí do textového pole
     */
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        self.settingsPicker.hidden = false
        print("textFieldShouldBeginEditing")
        if textField == self.durationTextField{
            self.activeTextField(1)
            self.settingsPicker.reloadAllComponents()
            self.settingsPicker.selectRow(Int(self.detailDurationHours), inComponent: 0, animated: true)
            self.settingsPicker.selectRow(Int(self.detailDurationMin), inComponent: 1, animated: true)
            self.settingsPicker.selectRow(Int(self.detailDurationSec), inComponent: 2, animated: true)
        }
        else if textField == self.pauseTextField{
            self.activeTextField(2)
            self.settingsPicker.reloadAllComponents()
            self.settingsPicker.selectRow(Int(self.detailPauseDurationHours), inComponent: 0, animated: true)
            self.settingsPicker.selectRow(Int(self.detailPauseDurationMin), inComponent: 1, animated: true)
            self.settingsPicker.selectRow(Int(self.detailPauseDurationSec), inComponent: 2, animated: true)
        }
        else if textField == self.repeatTextField {
            self.activeTextField(3)
            self.settingsPicker.reloadAllComponents()
            self.settingsPicker.selectRow(Int(self.detailRepeat), inComponent: 0, animated: true)
        }
        return false
    }
    
    /**
     Metoda pro nastavení aktivního textového pole
     */
    func activeTextField(index: Int){
        self.textFieldSelected = index
        let someOption = index
        switch someOption {
        case 1:
            self.durationTextField.backgroundColor = UIColor.lightGrayColor()
            self.pauseTextField.backgroundColor = UIColor.clearColor()
            self.repeatTextField.backgroundColor = UIColor.clearColor()
        case 2:
            self.durationTextField.backgroundColor = UIColor.clearColor()
             self.pauseTextField.backgroundColor = UIColor.lightGrayColor()
            self.repeatTextField.backgroundColor = UIColor.clearColor()
        case 3:
            self.durationTextField.backgroundColor = UIColor.clearColor()
            self.pauseTextField.backgroundColor = UIColor.clearColor()
            self.repeatTextField.backgroundColor = UIColor.lightGrayColor()
        default:
            self.durationTextField.backgroundColor = UIColor.clearColor()
            self.pauseTextField.backgroundColor = UIColor.clearColor()
            self.repeatTextField.backgroundColor = UIColor.clearColor()
        }
    
    }
    
    /**
     Načtení dat z mixu
     */
    func getValues(){
        //mix typu playlistů
        if self.mixType == 1{
            var mix:audioMix = gl.audioMixes.at(mixID)
            self.globalSettingsSwitch.on = mix.mixGlobalSettingsON
            //lokální nastavení
            if self.mixItemID != nil {
                let mixItem:audioMixItem = mix.mixQueue.at(mixItemID)
                self.setLocalDuration(mixItem.mixItemDuration)
                self.setLocalPauseDuration(mixItem.mixItemPauseDuration)
                self.detailRepeat = mixItem.mixItemRepeat
                self.title = "Nastavení: "+String(mixItem.mixItem["playlist"])
            }
            //globální nastavení
            else{
                self.setLocalDuration(mix.mixGlobalDuration)
                self.setLocalPauseDuration(mix.mixGlobalPauseDuration)
                self.detailRepeat = mix.mixGlobalItemRepeat
                self.title = "Globální nastavení: "+String(mix.mixName)
            }
        }
        //mix typu rádio streamů
        else if self.mixType == 2{
            var mix:radioMix = gl.radioMixes.at(mixID)
            self.globalSettingsSwitch.on = mix.mixGlobalSettingsON
            //lokální nastavení
            if self.mixItemID != nil {
                let mixItem:radioMixItem = mix.mixQueue.at(mixItemID)
                self.setLocalDuration(mixItem.mixItemDuration)
                self.setLocalPauseDuration(mixItem.mixItemPauseDuration)
                detailRepeat = mixItem.mixItemRepeat
                self.title = "Nastavení: "+String(mixItem.mixItem.sectionName)
            }
            //globální nastavení
            else{
                self.setLocalDuration(mix.mixGlobalDuration)
                self.setLocalPauseDuration(mix.mixGlobalPauseDuration)
                detailRepeat = mix.mixGlobalItemRepeat
                self.title = "Globální nastavení: "+String(mix.mixName)
            }
        }
    }
    
    /**
     Načtení dat o trvání mixu do lakolních proměnných
     */
    func setLocalDuration(duration: Float64){
        detailDurationHours = floor((duration / 3600) % 60)
        print("detailDurationHours: "+String(detailDurationHours))
        detailDurationMin = floor((duration / 60) % 60)
        print("detailDurationMin: "+String(detailDurationMin))
        detailDurationSec = floor(duration % 60)
        print("detailDurationSec: "+String(detailDurationSec))
    }
    
    /**
     Načtení dat o trvání pauzy mixu do lakolních proměnných
     */
    func setLocalPauseDuration(duration: Float64){
        detailPauseDurationHours = floor((duration / 3600) % 60)
        print("detailPauseDurationHours: "+String(detailPauseDurationHours))
        detailPauseDurationMin = floor((duration / 60) % 60)
        print("detailPauseDurationMin: "+String(detailPauseDurationMin))
        detailPauseDurationSec = floor(duration % 60)
        print("detailPauseDurationSec: "+String(detailPauseDurationSec))
    }
    
    /**
     Přenastavení dat do mixu
     */
    func setValues(){
        // typ mixu playlistů
        if mixType == 1{
            //var mix:audioMix = gl.audioMixes.items[mixID]
            if mixItemID != nil { //lokální nastavení
                gl.audioMixes.items[mixID].mixQueue.items[mixItemID].mixItemDuration = detailDurationHours*3600+detailDurationMin*60+detailDurationSec
                gl.audioMixes.items[mixID].mixQueue.items[mixItemID].mixItemPauseDuration = detailPauseDurationHours*3600+detailPauseDurationMin*60+detailPauseDurationSec
                gl.audioMixes.items[mixID].mixQueue.items[mixItemID].mixItemRepeat = detailRepeat
            }
            else{//globální nastavení
                gl.audioMixes.items[mixID].mixGlobalDuration = detailDurationHours*3600+detailDurationMin*60+detailDurationSec
                gl.audioMixes.items[mixID].mixGlobalPauseDuration = detailPauseDurationHours*3600+detailPauseDurationMin*60+detailPauseDurationSec
                gl.audioMixes.items[mixID].mixGlobalItemRepeat = detailRepeat
            }
            gl.audioMixes.items[mixID].mixGlobalSettingsON = globalSettingsSwitch.on
        }
            //typ mixu rádio streamů
        else if mixType == 2{
            if mixItemID != nil { //lokální nastavení
                gl.radioMixes.items[mixID].mixQueue.items[mixItemID].mixItemDuration = detailDurationHours*3600+detailDurationMin*60+detailDurationSec
                gl.radioMixes.items[mixID].mixQueue.items[mixItemID].mixItemPauseDuration = detailPauseDurationHours*3600+detailPauseDurationMin*60+detailPauseDurationSec
                gl.radioMixes.items[mixID].mixQueue.items[mixItemID].mixItemRepeat = detailRepeat
            }
            else{//globální nastavení
                gl.radioMixes.items[mixID].mixGlobalDuration = detailDurationHours*3600+detailDurationMin*60+detailDurationSec
                gl.radioMixes.items[mixID].mixGlobalPauseDuration = detailPauseDurationHours*3600+detailPauseDurationMin*60+detailPauseDurationSec
                gl.radioMixes.items[mixID].mixGlobalItemRepeat = detailRepeat
            }
            gl.radioMixes.items[mixID].mixGlobalSettingsON = globalSettingsSwitch.on
        }
    }
    
    /**
     Obnovení vzhledu nastavení podle typu mixu a typu nastavení
     */
    func updateView(){
        
        self.updateTextFields()
        
        if globalSettingsSwitch.on == false && mixItemID == nil{
            durationTextField.userInteractionEnabled = false
            durationTextField.textColor = UIColor.lightGrayColor()
            pauseTextField.userInteractionEnabled = false
            pauseTextField.textColor = UIColor.lightGrayColor()
            repeatTextField.userInteractionEnabled = false
            repeatTextField.textColor = UIColor.lightGrayColor()
        }
        else if globalSettingsSwitch.on == true && mixItemID == nil{
            durationTextField.userInteractionEnabled = true
            durationTextField.textColor = UIColor.blackColor()
            pauseTextField.userInteractionEnabled = true
            pauseTextField.textColor = UIColor.blackColor()
            repeatTextField.userInteractionEnabled = true
            repeatTextField.textColor = UIColor.blackColor()
        }
        else if globalSettingsSwitch.on == false && mixItemID != nil{
            durationTextField.userInteractionEnabled = true
            durationTextField.textColor = UIColor.blackColor()
            pauseTextField.userInteractionEnabled = true
            pauseTextField.textColor = UIColor.blackColor()
            repeatTextField.userInteractionEnabled = true
            repeatTextField.textColor = UIColor.blackColor()
        }
        else if globalSettingsSwitch.on == true && mixItemID != nil{
            durationTextField.userInteractionEnabled = false
            durationTextField.textColor = UIColor.lightGrayColor()
            pauseTextField.userInteractionEnabled = false
            pauseTextField.textColor = UIColor.lightGrayColor()
            repeatTextField.userInteractionEnabled = false
            repeatTextField.textColor = UIColor.lightGrayColor()
        }
    }
    
    /**
     Obnovení hodnot v textových polích, po změnách v uipickeru
     */
    func updateTextFields(){
        print("updateTextFields")
        durationTextField.text = String(format: "%02.0f:%02.0f:%02.0f", detailDurationHours, detailDurationMin, detailDurationSec)
        print("detailDurationHours: "+String(detailDurationHours))
        print("detailDurationMin: "+String(detailDurationMin))
        print("detailDurationSec: "+String(detailDurationSec))
        pauseTextField.text = String(format: "%02.0f:%02.0f:%02.0f", detailPauseDurationHours, detailPauseDurationMin, detailPauseDurationSec)
        repeatTextField.text = String(detailRepeat)
    }
    
}