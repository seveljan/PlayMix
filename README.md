PlayMix je hudební apliakce zaměřena na mixování playlistů v zařízení iOS pro podporu výuky společenského tance s doplňkem pro mixování(střídavý poslech) internetových streamů rádií.

Zadání:

Cílem práce je vytvořit mobilní aplikaci pro iOS, která bude kombinovat poslech playlistů ze zařízení dle
nastavení uživatele. Bude sloužit pro trénink společenského či sportovního tance.
Uživatel bude mít skladby roztříděné do playlistů podle tanců. Aplikace bude umět přehrávat skladby
o nastavené délce a náhodně vybírat skladby z vybraných playlistů. Mezi skladbami bude možnost nastavení
délky pauzy. Přehrávač bude umět přehrát po sobě např. všechny playlisty vždy po jedné skladbě z daného
playlistu. 

Pokyny:
- Proveďte analýzu jiných hudebních přehrávačů pro společenské tance z App Store.
- Proveďte analýzu požadavků ze strany potenciálních uživatelů.
- Navrhněte funkcionalitu aplikace a jednotlivé obrazovky ve formě wireframů.
- Navrhněte a implementujte aplikaci.
- Aplikaci otestujte